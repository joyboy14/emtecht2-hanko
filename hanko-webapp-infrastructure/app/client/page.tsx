import { NewClient } from "@/components/clients/NewClient";
import { ClientItem } from "@/components/clients/ClientItem";
import { prisma } from "@/db";
import { Navbar } from "@/components/navbar/Navbar";
import { DeleteAll } from "@/components/clients/DeleteAll";
import { userId } from "@/app/api/client/route";

export default async function Client() {

  
  const userID = await userId();

  const clients = await prisma.client.findMany({
    where: {
      userId: { equals: userID },
    },
  });

  return (
    <main className="bg-background-color flex flex-col min-h-screen justify-between text-base-color relative">
      <div className="top-8 flex justify-between items-center absolute w-full mx-auto px-6 lg:hidden ">
      </div>
      <Navbar />
      <section className="mx-auto my-10  ">
        <div className="relative bg-brand-contrast-color rounded-3xl py-6 mt-10 w-[350px] h-[400px] md:w-[450px]  flex flex-col">
          <h1 className="text-2xl md:text-3xl xl:text-4xl text-center text-white">
            List of my clients
          </h1>
          <NewClient />
          <ul className="mb-20 px-6  overflow-auto">
            <ClientItem clients={clients} />
          </ul>
          <div className="absolute bottom-6 xl:bottom-9 w-full justify-center flex">
            <DeleteAll clients={clients} />
          </div>
        </div>
      </section>
    </main>
  );
}
