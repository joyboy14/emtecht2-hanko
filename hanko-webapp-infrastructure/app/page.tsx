import dynamic from "next/dynamic";
import logo from './assets/logo.png';

const HankoAuth = dynamic(() => import('@/components/hankoComponents/Register'), { ssr: false })

const Home = () => {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen text-base-color">
    <div className="bg-gray-200 bg-opacity-30 rounded-lg shadow-md p-6">
      <img src={logo.src} className="w-24 h-24 mb-4 mx-auto" alt="Logo" />
      <h1 className="text-white text-4xl text-center mb-8">Client Manager</h1>
      <HankoAuth />
    </div>
  </div>
  
  );
};

export default Home;
