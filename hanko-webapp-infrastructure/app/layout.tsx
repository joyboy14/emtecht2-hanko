import "./globals.css";
import type { Metadata } from "next";
import bg from './assets/background.png';

const bgstyle= {
  backgroundImage: `url(${bg.src})`,
  backgroundSize: 'cover',
  backgroundPosition: 'center',
  width: '100%',
  height: '100%',
};

export const metadata: Metadata = {
  title: "Client Manager",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className="mx-auto" style={bgstyle}>{children}</body>
    </html>
  );
}
