'use client';

import { useUserData } from "@/hooks/useUserData";
import { useSessionData } from "@/hooks/useSessionData";

export default function UserData() {
  const { id, email, loading: userDataLoading, error: userDataError } = useUserData();
  const { userID, jwt, isValid, loading: sessionDataLoading, error: sessionDataError } = useSessionData();

  if (userDataLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="overflow-hidden w-50">
        <div className="mb-4 text-black">User id: <span className="text-white">{id}</span></div>
        <div className="text-black">{email}</div>
      
    </div>
  );
}
