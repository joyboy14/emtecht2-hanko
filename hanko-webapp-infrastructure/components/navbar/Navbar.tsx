'use client';

import { ProfileBtn } from "@/components/navbarBtns/ProfileBtn";
import { LogoutBtn } from "@/components/navbarBtns/LogoutBtn";
import Link from "next/link";
import logo from '../../app/assets/logo.png';
import { useUserData } from "@/hooks/useUserData";

export const Navbar = () => {
  const { id, email, loading: userDataLoading, error: userDataError } = useUserData();

  if (userDataLoading) {
    return <div>Loading...</div>;
  }


  return (
    <nav className="fixed top-0 left-0 right-0 border-color-shade-2 border-b bg-white bg-opacity-40 text-base-color pt-2 z-40 lg:block hidden">
      <div className="flex justify-between items-center px-4 lg:px-14 xl:px-56">
        <div className="flex items-center mr-4">
          <Link href="/">
            <img src={logo.src} className="w-12 h-12" alt="Logo" />
          </Link>
          <span className="ml-2 text-lg font-semibold text-white">Client Manager, Hello {email} </span>
        </div>
        <div className="flex gap-10 items-center">
          <div className="mb-2">
            <ProfileBtn />
          </div>
          <div className="mb-2">
            <LogoutBtn />
          </div>
        </div>
      </div>
    </nav>
  );
};



