"use client";

import { useState, useEffect, useCallback } from "react";
import { useRouter } from "next/navigation";
import { Hanko } from "@teamhanko/hanko-elements";

const hankoApi = process.env.NEXT_PUBLIC_HANKO_API_URL;

export function   LogoutBtn() {
  const router = useRouter();
  const [hanko, setHanko] = useState<Hanko>();
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    import("@teamhanko/hanko-elements").then(({ Hanko }) =>
      setHanko(new Hanko(hankoApi ?? ""))
    );
  }, []);

  const renewSession = useCallback(() => {
    router.replace("/");
  }, [router]);

  useEffect(
    () =>
      hanko?.onSessionExpired(() => {
        renewSession();
      }),

    [hanko, renewSession]
  );

  const logout = () => {
    hanko?.user
      .logout()
      .then(() => {
        router.push("/");
        router.refresh();
        return;
      })
      .catch(setError);
  };

  return (
    <button
      onClick={logout}>
      <svg className="h-8 w-8 inline-block ml-2 text-white" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1" />
        </svg>
    </button>
  );
}
