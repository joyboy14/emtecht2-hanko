"use client";
import { useState, useRef, useEffect } from "react";
import { classNames } from "@/lib/classNames";
import dynamic from "next/dynamic";

const HankoProfile = dynamic(() => import('@/components/hankoComponents/Profile'), { ssr: false });

export function ProfileBtn() {
  const [openState, setOpenState] = useState(false);
  const sectionRef = useRef(null);

  const profileClick = () => {
    setOpenState(!openState);
  };

  const handleClickOutside = (event) => {
    if (sectionRef.current && !sectionRef.current.contains(event.target)) {
      setOpenState(false);
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  return (
    <>
      <a
        href="#"
        className={classNames(openState && "open","")}
        onClick={profileClick}
      >
       <svg class="h-8 w-8 text-white"  fill="none" viewBox="0 0 24 24" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"/>
      </svg>

      </a>
      {openState && (
        <div className="lg:backdrop-blur-md lg:bg-background-color/30 z-30 lg:min-h-screen w-full absolute left-0 right-0 top-36 lg:top-24 flex justify-center items-center">
          <section ref={sectionRef} className="bg-gray-200 bg-opacity-30 w-[350px] sm:w-[420px] lg:w-[450px] h-auto mx-auto px-6 py-8 rounded-2xl z-50 lg:shadow-color-shade-1/10 shadow-md bg-brand-contrast-color border border-color-shade-2 overflow-auto" style={{ maxHeight: 'calc(100vh - 100px)' }}>
            <HankoProfile />
          </section>
        </div>
      )}
    </>
  );
}
