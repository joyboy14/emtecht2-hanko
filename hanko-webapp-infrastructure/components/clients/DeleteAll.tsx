"use client";

import { useRouter } from "next/navigation";
import { Client } from "@prisma/client";

export const DeleteAll = ({ clients }: { clients: Client[] }) => {
  const router = useRouter();
  const deleteAll = async (clients: Client[]) => {
    await fetch(`/api/client`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        clients,
      }),
    });
    router.refresh();
  };

  return (
    <button
      type="button"
      onClick={() => deleteAll(clients)}
      className="rounded-full bg-brand-color hover:bg-brand-color-shade-1 px-12 py-1 text-base-color shadow-sm font-IBM leading-7 xl:text-xl text-white"
    >
      Delete all my client
    </button>
  );
};
