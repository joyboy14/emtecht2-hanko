# EMTECHT2
Our project revolves around integrating the Hanko infrastructure into a Next.js application, specifically for passkey authentication. This implementation aims to fortify security measures and optimize the authentication process within our application, ensuring a seamless user experience.

The theory and procedures outlined in this document are derived from the documentation provided by Hanko, available at [https://docs.hanko.io/introduction](https://docs.hanko.io/introduction).

![alt text](./assets/project_login.png)


## What is Hanko
Hanko is an open-source solution for authentication and user management tailored for the passkey era. It provide concerns about weak or stolen passwords by providing authentication APIs and embeddable UI components. Integrating Hanko ensures better security and enhances user satisfaction.

## Hanko's features

Hanko presents some features such as : 

**User Authentication**

This feature enables users to securely verify their identity when accessing a system or application. It typically involves methods like username/password, biometric authentication, or multifactor authentication to ensure secure access (Passkeys, Social SSO, Passcodes, Security Key,...)

**Auth Component**

The authentication component provides the necessary infrastructure and functionality for implementing user authentication within a system or application. It includes features such as login forms, session management, and authentication workflows. They are ready-to-use components.

**Profile Component**

The profile component manages user profiles within the system or application. It allows users to view and edit their personal information, preferences, and settings.

**Passkey API**

The Passkey API facilitates the generation, management, and verification of passkeys, which are cryptographic keys used for authentication and access control. This API enables developers to integrate passkey-based authentication into their systems or applications for enhanced security.

# Passkeys
Passkeys are cryptographic keys used for authentication and access control. They offer enhanced security compared to traditional passwords by leveraging cryptographic algorithms. Passkeys can take various forms, such as randomly generated strings or biometric data, and are used to verify a user's identity before granting access to a system or application.

## How do Hanko and passkeys work ?
Passkeys work by utilizing pairs of cryptographic public and private keys. When a user registers their device with Hanko, a key pair is generated locally on the device. The public key is sent to Hanko to be associated with the user, while the private key remains solely on the user's device.

For integration with features such as Windows Hello or Touch ID on Mac, Hanko uses these biometric methods to authenticate the user and unlock their private key stored on the device. Once the user has been successfully authenticated, Hanko uses the private key to sign an authentication request, which is sent to the Hanko server for verification.

![alt text](./assets/passkeys_2.png)

Once the Hanko server has verified the signature with the public key associated with the user, it creates a secure session cookie that is returned to the user. This session cookie is used to identify the user during subsequent interactions with the server, thereby avoiding the need for reauthentication with each request.

![alt text](./assets/passkeys_1.png)

# Setting up a project on Hanko
Once your account is created on Hanko, go to the console, select "All Projects," and create a new project.

You have two types of projects : 

**Hanko Authentication and User Management** 

It's a comprehensive solution for onboarding and authenticating users, offering customizable UI components and an admin dashboard based on Hanko Infrastructure. 

**Passkey API** 

It's an infrastructure for integrating passkeys into applications, supporting creation, authentication, and transaction use cases with a managed FIDO2-certified WebAuthn server API.

![alt text](./assets/hanko_project_new_1.png)

> Select **Hanko**

For example, let's create a Hanko project tailored for a Next.js application with the specified parameters.

![alt text](./assets/hanko_project_new_2.png)

After a little moment, you can navigate to the dashboard. There, you can retrieve the API URL and set it as the value for NEXT_PUBLIC_HANKO_API_URL in the .env file for further integration with your Next.js application.

```bash
#.env
DATABASE_URL="file:./dev.db"
NEXT_PUBLIC_HANKO_API_URL=$YOUR_API_VALUE
```

# Setting up our example

0. Clone our repository using the command `git clone https://gitlab.com/joyboy14/emtecht2-hanko.git`.

1. Open the terminal in the `hanko-webapp-infrastructure` directory.
    Create a `.env` file and enter these two variables:
    ```dotenv
    # .env
    DATABASE_URL="file:./dev.db"
    NEXT_PUBLIC_HANKO_API_URL=$YOUR_API_VALUE (Obtained from hanko.io)
    ```

2. Run the command `npm install` to install the dependencies.

3. Setting up Prisma database:
    Run the command `npx prisma generate`.
    Then run the command `npx prisma migrate`.

4. Finally, run the command `npm run dev`. The project will be available at "http://localhost:3000".

```bash
npm install
npx prisma generate
npx prisma migrate
npm run dev
```


## Overview of Example Functionality

### Page localhost:3000/

On this page, the authentication process occurs using Hanko's authentication component. If you are already registered, you can directly "sign in with a Passkey". Otherwise, you need to enter an email address to sign up. You will then receive an email with an authorization code to create and register your Passkey.

Upon signing in with the Passkey, you will be authenticated and redirected to the "/client" page, which is only accessible after authentication.

### Client Page localhost:3000/client

On this page, you can create new clients.

### Profile Options

By clicking on the profile icon at the top right, you can register new email addresses for your login and also modify your Passkey or create a new Passkey. If you choose the "use another device" option, you can use your iPhone, iPad, or Android device for your next logins. You will then have a QR code to scan with your device and use your device's biometric authentication to use the new Passkey, this time stored on your iPhone, for example.

### Logout

You can also log out using the "logout" icon at the top right.


# Internal Components of Hanko

## Register Component (Components/hankoComponents/Register.tsx)

Located in the `Components/hankoComponents` directory, the `Register.tsx` file contains code for the registration component of Hanko.

### Code Explanation:
```typescript
"use client";
```
- The `use client` annotation indicates that the component uses the Hanko client object for interacting with the Hanko API.

- This component sets up authentication using Hanko's authentication component. It imports necessary hooks and components from React and Next.js.

#### Define event callbacks :

```typescript
useEffect(
  () =>
    hanko?.onAuthFlowCompleted(() => {
      redirectAfterLogin();
    }),
  [hanko, redirectAfterLogin]
);
```
This part of the code defines a callback function to handle the completion of the authentication flow. It listens for the onAuthFlowCompleted event triggered by the Hanko client.
When the authentication flow is completed successfully, the redirectAfterLogin function is called to redirect the user to the /client page.

## Profile Component (Components/hankoComponents/Profile.tsx)

Located in the `Components/hankoComponents` directory, the `Profile.tsx` file contains code for the profile component of Hanko.

The `<hanko-profile>` component provides an interface where users can manage their email addresses and passkeys.

#### Code Explanation:

- This component sets up registration using Hanko's registration component. It imports necessary hooks and components from React.
- It initializes the Hanko API for managing email addresses and passkeys.


## Logout Button Component (Components/navbarBtns/LogoutBtn.tsx)

Located in the `Components/navbarBtns` directory, the `LogoutBtn.tsx` file contains code for the logout button component.

The LogoutBtn component implements logout functionality using @teamhanko/hanko-elements. It provides a reusable logout button component that can be used anywhere in the application.

#### Code Explanation:

This component sets up logout functionality using Hanko's elements. It imports necessary hooks and components from React.
It initializes the Hanko API for managing logout functionality.

## Middleware for Authentication (middleware.ts)

The `middleware.ts` file contains code responsible for authenticating requests in the project.

The middleware ensures that incoming requests to protected routes (`/client` and its subroutes) are authenticated using Hanko JWT tokens. If a token is invalid or expired, the user is redirected to the root URL.

### Code Explanation:
- The code begins with importing necessary modules and packages such as `NextResponse` and `NextRequest` from `next/server`, as well as `jose` for handling JSON Web Tokens (JWT).
- It retrieves the Hanko API URL from the environment variables.
- The `middleware` function is defined with a parameter `req` representing the incoming request.
- It extracts the Hanko JWT token from the cookies of the incoming request.
- It creates a remote JSON Web Key Set (JWKS) using the Hanko API URL.
- The Hanko JWT token is then verified using the JWKS.
- If the verification fails, indicating an invalid or expired token, the middleware redirects the request to the root URL.
- The `config` object specifies the URLs to which the middleware should be applied. In this case, it applies to all routes starting with `/client`.


## Custom Hooks for User and Session Data Retrieval (hooks/useUserData.ts && hooks/useSessionData.ts)

The `useSessionData.ts` and `useUserData.ts` files contain custom hooks responsible for fetching user and session data in the project.

### Client Side

These hooks provide convenient methods for accessing user-specific information and managing session-related tasks within React components.

#### useUserData

The `useUserData` hook retrieves the current user's data by utilizing the `hanko.user.getCurrent()` method from `@teamhanko/hanko-elements`. This hook is particularly useful for fetching user details like user ID and email.

#### useSessionData

On the other hand, the `useSessionData` hook is designed to handle session data. It employs the `hanko.session.isValid()` method from `@teamhanko/hanko-elements` to check the validity of the current session. This hook is essential for managing session-related functionalities and ensuring the security of user sessions.

These custom hooks streamline the process of accessing user and session data within React components, facilitating seamless integration of authentication and authorization features into the application logic.

## User and Session Data Retrieval Component (components/hankoComponents/userData.ts)

The `userData.ts` file within the `components/hankoComponents` directory serves as a demonstration component for retrieving user and session information.

This component utilizes the `useUserData` custom hook, which is designed to fetch user data from the Hanko API. It employs the `hanko.user.getCurrent()` method provided by `@teamhanko/hanko-elements` to retrieve data of the currently authenticated user.

The `useUserData` hook manages loading and error states during the user data retrieval process. It returns an object containing the user's ID, email, as well as loading and error indicators.

This demonstration component is valuable for showcasing how to integrate user data retrieval into your application using the features provided by the Hanko API. It also serves as a starting point for further exploration and customization of authentication and authorization integration in your project.


The final example of its usage can be seen in the Navbar.tsx file located in the components/navbar directory. In this file, the user's email is displayed as "Hello, email".


# Conclusion
In summary, the incorporation of Hanko's passkey authentication not only fortifies our application's security but also simplifies the user experience through its intuitive UI. This combination ensures heightened protection against unauthorized access while maintaining ease of use for our users. And it's enjoyable to use.



